open Batteries

let word_regex = Re_pcre.regexp "\\w"
let space_regex = Re_pcre.regexp "^\\s+$"
let word_separator_regex = Re_pcre.regexp "\\w+:\\S+|[\\w'-]+|[^\\w\\s][^\\w]*[^\\w\\s]|[^\\w\\s]|\\s+"
let word_separator_text = Text.of_string " "
let word_separator_string = " "
let text_to_string =
  Text.to_string

let string_to_text =
  Text.of_string

let textify tokens =
  Text.concat word_separator_text tokens

let stringify tokens =
  String.concat word_separator_string tokens

let tokenize phrase =
  let stripped = Text.strip phrase in
  match Text.is_empty stripped with
  | true -> []
  | false ->
    Re_pcre.split word_separator_regex (text_to_string stripped)

let is_word text =
  Re_pcre.pmatch word_regex text

let is_space text =
  Re_pcre.pmatch space_regex text

let count tokens =
  let rec count_tokens acc tokens =
    match tokens with
    | [] -> acc
    | tkn::rest ->
      let spc = is_space tkn in
      match spc with
      | true -> count_tokens acc rest
      | false -> count_tokens (acc + 1) rest
  in
  count_tokens 0 tokens

