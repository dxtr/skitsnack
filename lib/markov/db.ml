open Batteries
open Lwt.Infix

module Lwt_thread = struct
  include Lwt
  let close_in = Lwt_io.close
  let really_input = Lwt_io.read_into_exactly
  let input_binary_int = Lwt_io.BE.read_int
  let input_char = Lwt_io.read_char
  let output_string = Lwt_io.write
  let output_binary_int = Lwt_io.BE.write_int
  let output_char = Lwt_io.write_char
  let flush = Lwt_io.flush
  type out_channel = Lwt_io.output_channel
  type in_channel = Lwt_io.input_channel
  let open_connection sockaddr = Lwt_io.open_connection sockaddr
end

module Lwt_PGOCaml = PGOCaml_generic.Make(Lwt_thread)

type pool = (string,bool) Hashtbl.t Lwt_PGOCaml.t Lwt_pool.t

exception Insert_Token of string
exception Insert_Node of int32 list
    
let bool_oid = Int32.of_int 16
let int4_oid = Int32.of_int 23
let text_oid = Int32.of_int 25

let validate db =
  try%lwt
    let%lwt () = Lwt_PGOCaml.ping db in
    Lwt.return_true
  with _ ->
    Lwt.return_false

type all_token_expr =
  { fields : string list;
    expr : string;
    seq_expr : string;
    args : string;
    arg_types : Lwt_PGOCaml.oid list
  }

let get_all_token_expr order =
  let field_nums = List.range 0 `To (order-1) in
  let fields = List.map (fun x -> Printf.sprintf "token%d_id" x) field_nums in
  let args = String.join " AND " (List.map2 (fun num field -> Printf.sprintf "%s = $%d" field (num+1)) field_nums fields) in
  let seq_expr = String.join "," (List.map (fun num -> Printf.sprintf "$%d" (num+1)) field_nums) in
  { fields = fields;
    expr = String.join "," fields;
    seq_expr = seq_expr;
    args = args;
    arg_types = List.make order int4_oid;
  }

let get_last_token order =
  String.join "" ["token";Int.to_string (order-1);"_id"]

let connect ~host ~port ~user ~password ~database () =
  let markov_order = 3 in
  Lwt_PGOCaml.connect ~host ~port ~user ~password ~database () >>= fun dbh ->
  Lwt_PGOCaml.set_private_data dbh @@ Hashtbl.create 10;
(* TODO: Get order from the database *)
  let token_expr = get_all_token_expr markov_order in
  let last_token = get_last_token markov_order in

  let query = Printf.sprintf "SELECT id FROM node WHERE %s" token_expr.args in
  let name = "get_node_by_tokens" in
  let types = token_expr.arg_types in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = Printf.sprintf "INSERT INTO node (count,%s) VALUES (0,%s) RETURNING id" token_expr.expr token_expr.seq_expr in
  let name = "insert_node" in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = Printf.sprintf "SELECT token.text, edge.has_space FROM node,edge,token WHERE edge.id = $1 AND edge.prev_node = node.id AND node.%s = token.id" last_token in
  let name = "get_text_by_edge" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT get_random_token() AS id" in
  let name = "get_random_token" in
  let types = [] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT get_random_node_with_token($1) AS id" in
  let name = "get_random_node_with_token" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT edge.count AS ecount, node.count AS ncount FROM edge,node WHERE edge.id = $1 AND edge.prev_node = node.id" in
  let name = "get_edge_count" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "INSERT INTO edge (prev_node, next_node, has_space, count) VALUES ($1,$2,$3,1) ON CONFLICT ON CONSTRAINT prev_next_uniq DO UPDATE SET count = excluded.count + 1 RETURNING id" in
  let name = "add_edge" in
  let types = [int4_oid;int4_oid;bool_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT text FROM token WHERE id = $1" in
  let name = "get_token" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT id FROM token WHERE text = $1" in
  let name = "get_token_by_text" in
  let types = [text_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT (get_random_next_from_edge($1)).*" in
  let name = "get_random_next_from_edge" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "SELECT (get_random_prev_from_edge($1)).*" in
  let name = "get_random_prev_from_edge" in
  let types = [int4_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->

  let query = "INSERT INTO token (text, is_word) VALUES ($1,$2) RETURNING id" in
  let name = "insert_token" in
  let types = [int4_oid;bool_oid] in
  print_endline query;
  Lwt_PGOCaml.prepare dbh ~query ~name ~types () >>= fun () ->
  
  Lwt.return dbh

let init ~host ~port ~user ~password ~database ?pool_size () =
  let size =
    match pool_size with
    | None -> 16
    | Some n -> n
  in
  Lwt_pool.create size ~validate (connect ~host ~port ~user ~password ~database)

let close handle =
  Lwt_PGOCaml.close handle

let handle_id ~result ?on_empty ~fail_msg () =
  match result with
  | (id::_)::_ -> Option.get id |> Int32.of_string |> Lwt.return
  | []::_ -> Lwt.fail_with fail_msg
  | [] ->
    match on_empty with
    | None -> Lwt.fail_with fail_msg
    | Some f -> f ()

let insert_token dbpool text is_word =
  let is_word_str = Bool.to_string is_word in
  let params = [Some text;Some is_word_str] in
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"insert_token" ~params () >>=
       fun result ->
       handle_id ~result
         ~fail_msg:"insert_token" ())

let get_token_by_text dbpool (text:string) =
  let params = [Some text] in
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"get_token_by_text" ~params () >>=
       fun result ->
       handle_id ~result
         ~on_empty:(fun () -> insert_token dbpool text (Tokenizer.is_word text))
         ~fail_msg:"get_token_by_text" ())

let get_token dbpool token_id =
  let params = [Some (Int32.to_string token_id)] in
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"get_token" ~params () >>=
       fun result ->
       match result with
       | (txt::_)::_ -> Lwt.return txt
       | _ -> Lwt.return_none)

let get_tokens dbpool token_ids =
  match token_ids with
  | [] -> Lwt.return []
  | _ ->
    Lwt_pool.use dbpool
      (fun dbh ->
         let p = List.init ((List.length token_ids)-1) (fun x -> "$" ^ String.of_int (x+1))
                 |> String.concat ","
         in
         let query = "SELECT id FROM token WHERE id IN (" ^ p ^ ")" in
         let types = List.map (fun _ -> int4_oid) token_ids in
         let params = List.map (fun x -> Some (Int32.to_string x)) token_ids in
         Lwt_PGOCaml.prepare dbh ~query ~types () >>= fun () ->
         Lwt_PGOCaml.execute dbh ~params () >>= fun result ->
         match result with
         | [] -> Lwt.return []
         | _ ->
           Lwt.return @@ List.map
               (fun row ->
                  match row with
                  | [Some id] -> Some (Int32.of_string id)
                  | _ -> None)
               result)


let get_word_tokens dbpool token_ids =
  match token_ids with
  | [] -> Lwt.return []
  | _ ->
    Lwt_pool.use dbpool
      (fun dbh ->
         let p = String.concat "," @@ List.init ((List.length token_ids)-1) (fun x -> "$" ^ String.of_int (x+1)) in
         let query = "SELECT id FROM token WHERE id IN (" ^ p ^ ") AND is_word = true" in
         let types = List.map (fun _ -> int4_oid) token_ids in
         let params = List.map (fun x -> Some (Int32.to_string x)) token_ids in
         Lwt_PGOCaml.prepare dbh ~query ~types () >>= fun () ->
         Lwt_PGOCaml.execute dbh ~params () >>= fun result ->
         match result with
         | [] -> Lwt.return []
         | _ -> Lwt.return @@ List.map
             (fun row ->
                match row with
                | [Some id] -> Some (Int32.of_string id)
                | _ -> None)
             result)

let get_tokens_by_text dbpool texts =
  Lwt_list.map_s (fun txt -> get_token_by_text dbpool txt) texts
    
let insert_node dbpool order token_ids =
  (* Can we use PGSQL with this one? I think not. *)
  let params = List.map (fun x -> Some (Int32.to_string x)) token_ids in
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"insert_node" ~params () >>=
       fun result ->
       handle_id ~result ~fail_msg:"get_node_by_tokens" ())

let get_node_by_tokens dbpool order token_ids =
  (* Can we use PGSQL with this one? I think not. *)
  let params = List.map (fun x -> Some (Int32.to_string x)) token_ids in
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"get_node_by_tokens" ~params () >>=
       fun result ->
       handle_id ~result ~on_empty:(fun () -> insert_node dbpool order token_ids) ~fail_msg:"get_node_by_tokens" ())

let get_text_by_edge dbpool order edge_id =
  let params = [Some edge_id] in
  Lwt_pool.use dbpool (fun dbh -> Lwt_PGOCaml.execute dbh ~name:"get_text_by_edge" ~params ())

let get_random_token dbpool =
  Lwt_pool.use dbpool
    (fun dbh ->
       Lwt_PGOCaml.execute dbh ~name:"get_random_token" ~params:[] () >>=
       fun result ->
       match result with
       | [[Some id]] -> Lwt.return @@ Int32.of_string id
       | _ -> Lwt.fail_with "get_random_token")

let get_random_node_with_token dbpool token_id =
  let params = [token_id] in
  Lwt_pool.use dbpool (fun dbh ->
      Lwt_PGOCaml.execute dbh ~name:"get_random_node_with_token" ~params () >>=
      fun result ->
      match result with
      | (Some id::_)::_ -> Lwt.return @@ Int32.of_string id
      | _ -> Lwt.fail_with "get_random_node_with_token")

let get_edge_logprob dbpool edge_id =
  let params = [edge_id] in
  Lwt_pool.use dbpool
    (fun dbh -> Lwt_PGOCaml.execute dbh ~name:"get_edge_count" ~params () >>=
      fun select_result ->
      Lwt.return @@ match List.hd(select_result) with
      | [Some ecount;Some ncount] ->
        (Float.of_string ecount,Float.of_string ncount)
      | _ ->
        (-1.,-1.))

let add_edge dbpool prev next has_space =
  let prev_str = Int32.to_string prev in
  let next_str = Int32.to_string next in
  let has_space_str = Bool.to_string has_space in
  let params = [Some prev_str;Some next_str;Some has_space_str] in
  Lwt_pool.use dbpool (fun dbh ->
      Lwt_PGOCaml.execute dbh ~name:"add_edge" ~params () >>=
      function
      | (Some id::_)::_ -> Lwt.return id
      | _ -> Lwt.fail_with "add_edge")

type direction = Next | Prev

let search_random_walk dbpool start_id end_id direction =
  let stmt = match direction with
    | Next -> "get_random_next_from_edge"
    | Prev -> "get_random_prev_from_edge"
  in
  Lwt_pool.use dbpool (fun dbh ->
      let rec get_paths q acc =
        match q with
        | [] -> Lwt.return acc
        | (cur,path)::new_q ->
          let cur_str = Int32.to_string cur in
          let params = [Some cur_str] in
          Lwt_PGOCaml.execute dbh ~name:stmt ~params () >>=
          fun result ->
          match result with
          | [] -> Lwt.fail_with "search_random_walk -> get_paths"
          | row::_ ->
            match row with
            | [eid_str;node_str] ->
              let eid = Int32.of_string @@ Option.get eid_str in
              let new_path = path @ [eid] in
              let nxt = Int32.of_string @@ Option.get node_str in
              if end_id = nxt then
                get_paths new_q (acc @ new_path)
              else
                get_paths (new_q @ [(nxt,new_path)]) acc
            | _ -> Lwt.fail_with "get_paths"
      in
      get_paths [(start_id,[])] [])
