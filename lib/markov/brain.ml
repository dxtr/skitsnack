open Batteries
open Lwt.Infix

type token = (int32 * string)
type brain =
  { dbpool : Db.pool;
    markov_order : int;
    loop_time : int;
    end_token : int32;
    end_context : int32 array;
    end_context_id : int32;
  }

let end_token = ""
let space_token_id = -1l

let token_id (id,_) = id
let token_text (_,txt) = txt

let get_token_by_text brain text =
  if Tokenizer.is_space text then
    Lwt.return space_token_id
  else
    Db.get_token_by_text brain.dbpool text

let get_tokens brain ids =
  Db.get_tokens brain.dbpool ids

let add_edge brain prev_id next_id has_space =
  Db.add_edge brain.dbpool prev_id next_id has_space

let get_node_by_tokens brain tokens =
  Db.get_node_by_tokens brain.dbpool brain.markov_order tokens

let init order time =
  (* TODO: Move loop_time to a settings table in the database *)
  let dbpool = Db.init ~host:"localhost" ~port:5432 ~user:"skitsnack" ~password:"skitsnack" ~database:"skitsnack" ~pool_size:16 () in
  Db.get_token_by_text dbpool end_token >>=
  fun end_token ->
  let end_context = Array.create order end_token in
  List.make order end_token
  |> Db.get_node_by_tokens dbpool order >>=
  fun end_context_id ->
  Lwt.return { dbpool = dbpool;
               markov_order = order;
               loop_time = time;
               end_token = end_token;
               end_context = end_context;
               end_context_id = end_context_id;
             }

let to_edges brain tokens =
  let end_context = Array.to_list brain.end_context in
  let chain = List.concat [end_context;
                           tokens;
                           end_context]
  in
  let rec reduce_tokens has_space context acc tokens =
    match tokens with
    | [] -> acc
    | token::tail ->
      let new_context = context @ [token] in
      if (List.length new_context) <> brain.markov_order then
        reduce_tokens has_space new_context acc tail
      else if token == space_token_id then
        reduce_tokens true context acc tail
      else
        reduce_tokens false (List.tl new_context) (acc @ [new_context, has_space]) tail

  in
  Lwt.return @@ reduce_tokens false [] [] chain

let to_graph brain contexts =
  let rec reduce_edges prev acc ctx =
    match ctx with
    | [] -> acc
    | (ctx,has_space as context)::rest ->
      let new_prev = Some context in
      match prev with
      | None ->
        reduce_edges new_prev acc rest
      | Some (prev_ctx,prev_has_space) ->
        reduce_edges new_prev (acc @ [prev_ctx,has_space,ctx]) rest
  in
  Lwt.return @@ reduce_edges None [] contexts

let filter_pivots brain pivots =
  Db.get_word_tokens brain.dbpool pivots >>=
  fun result ->
  Lwt.return @@ List.map Option.get result

let learn_tokens brain tokens =
  let rec reduce prev_id (edges,count as acc) graph =
    match graph with
    | [] -> Lwt.return acc
    | (prev,has_space,next)::rest ->        
      let pid_promise =
        match prev_id with
        | None -> get_node_by_tokens brain prev
        | Some p -> Lwt.return p
      in
      let next_id_promise = get_node_by_tokens brain next in
      pid_promise >>= fun pid ->
      next_id_promise >>= fun next_id ->
      add_edge brain pid next_id has_space >>= fun new_edge ->
      reduce (Some next_id) (edges @ [new_edge], count + 1) rest
  in
  if Tokenizer.count tokens >= brain.markov_order then
    Lwt_list.map_s (get_token_by_text brain) tokens >>=
    to_edges brain >>=
    to_graph brain >>=
    reduce None ([],0)
  else Lwt.return ([],0)

let learn brain text =
  Tokenizer.tokenize text
  |> learn_tokens brain

let get_random_token brain =
  Db.get_random_token brain.dbpool

let babble brain ?num () =
  let n =
    match num with
    | None -> 5
    | Some n -> n
  in
  Lwt_list.map_s
    (fun _ ->
       get_random_token brain >>=
       fun tkn -> Option.get tkn |> Lwt.return)
    (List.make n "")

let pick_pivot pivot_ids =
  List.length pivot_ids |> Random.int |> List.nth pivot_ids

let generate_replies brain pivot_ids =
  let pivot_id = pick_pivot pivot_ids in
  let%lwt node = Db.get_random_node_with_token brain.dbpool pivot_id in
  let next_parts = Db.search_random_walk brain.dbpool node brain.end_context_id Db.Next in
  let prev_parts = Db.search_random_walk brain.dbpool node brain.end_context_id Db.Prev in
  let%lwt next_parts' = next_parts in
  let%lwt prev_parts' = prev_parts in
  List.map2 (fun nxt prv -> (nxt,prv)) next_parts' prev_parts'
  |> Lwt.return

let get_edge_logprob brain edge =
  let ln2 x = x /. Float.ln2 in
  match%lwt result = Db.get_edge_logprob brain.dbpool edge with
  | (ecount,ncount) -> (log ecount |> ln2) -. (log ncount |> ln2)

let reply brain text ?max_len () =
  let too_long reply =
    let text = Tokenizer.textify reply in
    match max_len with
    | Some mxlen -> Text.length text > mxlen
    | None -> false
  in
  let tokens = Tokenizer.tokenize text in
  Lwt_list.map_s (get_token_by_text brain) tokens >>=
  filter_pivots brain >>=
  fun ps ->
  let pivot_set = match ps with
    | [] -> babble brain ()
    | _ -> pivot_set
  in
  let replies = generate_replies brain pivot_set in
  let get_replies repl best_reply best_score acc =
    match repl with
    | [] -> acc
    | (edges,pivot_node)::tail ->
      let key = edges in
      
