open Batteries
open Lwt.Infix
open Printf

let main =
  let%lwt brain = Markov.Brain.init 3 500 in
  Markov.Brain.babble brain () >>= fun text ->
  Lwt.return @@ Printf.printf "%s\n" @@ Text.to_string text

let () = Lwt_main.run main
